# use lable and attribute to retrieve the data

import requests
from bs4 import BeautifulSoup

res = requests.get('https://www.google.com/finance') 
html = res.text
soup = BeautifulSoup( html,'html.parser') 

# print out the html content retrieved for specified label
items = soup.find_all(class_='ML43Jb S7JRS')
for item in items:
    print('Data that been retrieved：\n',item) 
    print(type(item))