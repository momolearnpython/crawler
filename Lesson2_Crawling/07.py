import requests
from bs4 import BeautifulSoup 
res =requests.get('https://www.google.com/finance')

html=res.text
soup = BeautifulSoup( html,'html.parser')

# retrieve sidebar currencies
items = soup.find_all(class_='ML43Jb S7JRS')

# get all currencies
for item in items: 
    # currency
    currency = item.find(class_ = 'j7FfMb') 
    # rate
    rate = item.find(class_='maIvLb').find('span').find('span')

    print(currency.text,rate.text,'\n')
