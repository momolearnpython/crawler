# usage of find() method

import requests
from bs4 import BeautifulSoup

url = 'https://www.google.com/finance'
res = requests.get (url)
print(res.status_code)

soup = BeautifulSoup(res.text,'html.parser')
item = soup.find('div') 
print(type(item)) 
print(item)  