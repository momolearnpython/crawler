# usage of find_all() method

import requests
from bs4 import BeautifulSoup

url = 'https://www.google.com/finance'
res = requests.get (url)
print(res.status_code)

soup = BeautifulSoup(res.text,'html.parser')
items = soup.find_all('div') 
print(type(items)) 
print(items)      