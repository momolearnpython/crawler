# Get Travel related books from "Books to Scrape"
# Print out book name, rating and price information. 
# http://books.toscrape.com/catalogue/category/books/travel_2/index.html

import requests 
from bs4 import BeautifulSoup 
res =requests.get('http://books.toscrape.com/catalogue/category/books/travel_2/index.html')
html=res.text
soup = BeautifulSoup(html,'html.parser')

items = soup.find_all(class_='product_pod')
for item in items:
    print('=======divider======') 
    print('Book Name: ',item.find('h3').find('a')['title']) 
    print('Rating: ',item.find('p')['class'][1])
    print('Price: ',item.find(class_='price_color').text)

