# Crawl info from "Books to Scrape" and print out the category information  
# example: Travel, Mystery, Historical Fiction.
# http://books.toscrape.com/

import requests 
from bs4 import BeautifulSoup 
res =requests.get('http://books.toscrape.com/')

html=res.text
soup = BeautifulSoup(html,'html.parser')

items = soup.find(class_='nav nav-list').find('ul').find_all('li')
for item in items:                    
    print(item.text.strip())