# Crawl goodreads the 20 best books of 2019
# Print the category and book name

import requests 
from bs4 import BeautifulSoup 
res =requests.get('https://www.goodreads.com/choiceawards/best-books-2019')

html=res.text
soup = BeautifulSoup(html,'html.parser')

# get the section
items = soup.find_all(class_='category clearFix')

for item in items:
    # category
    cat = item.find(class_='category__copy')
    print('Category:',cat.text)
    # book name
    bookname = item.find(class_='category__winnerImageContainer').find('img')
    print('Name:',bookname['alt'],'\n')