# Retrieve information based on lable and attribute

import requests 
from bs4 import BeautifulSoup 

# use Google Finance as an example

res = requests.get('https://www.google.com/finance')
html = res.text
soup = BeautifulSoup( html,'html.parser') 
items = soup.find_all(class_='ML43Jb S7JRS') 

print(items) 
print(type(items)) 